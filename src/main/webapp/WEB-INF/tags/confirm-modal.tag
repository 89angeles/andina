<%@tag description="Confirm modal tag." pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="modalMessage" required="false" type="java.lang.String"%>
<%@attribute name="modalTitle" required="false" type="java.lang.String"%>

<!-- Confirm Modal -->
<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">
                    <core:out value="${empty modalTitle ? 'Eliminar registro' : modalTitle}" />
                </h4>
            </div>
            <div class="modal-body">
                <core:out value="${empty modalMessage ? '¿Está seguro de eliminar este registo?' : modalMessage}" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a href="#" class="btn btn-danger danger">Eliminar</a>
            </div>
        </div>
    </div>
</div>