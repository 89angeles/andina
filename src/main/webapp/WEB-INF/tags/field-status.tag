<%@tag description="Status tag." pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="status" required="true" type="java.lang.String"%>

<core:choose>
    <core:when test="${status == 1}">
        <span class="success">Activo</span>
    </core:when>
    <core:when test="${status == 2}">
        <span class="warning">Inactivo</span>
    </core:when>
    <core:otherwise>
        <span class="danger">Eliminado</span>
    </core:otherwise>
</core:choose>
