<%@tag description="Status tag." pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="role" required="true" type="java.lang.String"%>

<core:choose>
    <core:when test="${role == 1}">
        <small>Administrador</small>
    </core:when>
    <core:when test="${role == 2}">
        <small>Oficial de Crédito</small>
    </core:when>
    <core:when test="${role == 3}">
        <small>Soporte</small>
    </core:when>
    <core:otherwise>
        &nbsp;
    </core:otherwise>
</core:choose>
