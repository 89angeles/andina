<%@tag description="Status tag." pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="role" required="true" type="java.lang.String"%>


<core:choose>
    <core:when test="${role == 1}">
        <span class="subtext">Administrador</span>
    </core:when>
    <core:when test="${role == 2}">
        <span class="subtext">Oficial de Crédito</span>
    </core:when>
    <core:when test="${role == 3}">
        <span class="subtext">Soporte</span>
    </core:when>
    <core:otherwise>
        &nbsp;
    </core:otherwise>
</core:choose>
