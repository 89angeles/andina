<%@tag description="Extended form-actions tag." pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="url" required="true" type="java.lang.String"%>

<%@attribute name="send" required="false" type="java.lang.Boolean"%>

<div class="form-group form-actions">
    <div class="col-sm-offset-2 col-sm-10">
        <a href="${url}" class="btn btn-default">Cancelar</a>
        <button type="submit" class="btn btn-success">${send ? 'Enviar' : 'Guardar'}</button>
    </div>
</div>
