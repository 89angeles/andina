<%@tag description="Extended input tag to allow for sophisticated errors" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="path" required="true" type="java.lang.String"%>
<%@attribute name="cssClass" required="false" type="java.lang.String"%>
<%@attribute name="label" required="false" type="java.lang.String"%>
<%@attribute name="required" required="false" type="java.lang.Boolean"%>
<core:if test="${empty label}">
    <core:set var="label" value="${fn:toUpperCase(fn:substring(path, 0, 1))}${fn:toLowerCase(fn:substring(path, 1,fn:length(path)))}" />
</core:if>
<spring:bind path="${path}">
    <div class="form-group ${status.error ? 'error' : ''}">
        <label class="col-sm-2 col-lg-2 control-label">${label}</label>
        <div class="col-sm-10 col-md-8">
            <form:password path="${path}"
                        cssClass="${empty cssClass ? 'form-control' : cssClass}"
                        required="${required ? 'required' : ''}" />
        </div>
    </div>
</spring:bind>
