<%@tag description="Extended form-actions tag." pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="id" required="true" type="java.lang.String"%>
<%@attribute name="baseUrl" required="true" type="java.lang.String"%>


<div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-gear"></i>&nbsp;<span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a href="${baseUrl}/${id}"><i class="fa fa-edit"></i>&nbsp;Editar</a></li>
        <li><a data-href="${baseUrl}/${id}/delete" data-toggle="modal" data-target="#confirm-modal" href="#"><i class="fa fa-times"></i>&nbsp;Eliminar</a></li>
    </ul>
</div>
