<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="default">
    <tiles:putAttribute name="body">
        <security:authorize access="isAuthenticated()" />
        <table id="datatable" class="table table-hover">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <core:forEach var="template" items="${templates}">
                <tr>
                    <td>${template.name}</td>
                    <td style="width: 10%;">
                        <tag:actions-button baseUrl="/templates" id="${template.id}" />
                    </td>
                </tr>
            </core:forEach>
        </table>
        <tag:confirm-modal />
    </tiles:putAttribute>
</tiles:insertDefinition>