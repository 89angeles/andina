<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-template" class="form-horizontal" method="post" action="/templates/save" role="form">
            <form:hidden path="template.id" />
            <tag:input path="template.name"  label="Nombre"
                       required="true" />
            <tag:input path="template.subject"  label="Asunto"
                       required="true" />
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Mensage</label>
                <div class="col-sm-10 col-md-8">
                    <textarea class="form-control" rows="7" name="message" data-rule-required="true">${template.message}</textarea>
                    <p class="help-block">Marcadores disponibles: {{missing-documents}}.</p>
                </div>
            </div>
            <tag:form-actions url="/templates" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>