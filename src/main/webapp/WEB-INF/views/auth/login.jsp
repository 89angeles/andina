<!DOCTYPE html>

<%@ page pageEncoding="UTF-8" %>

<!-- Imports -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>


<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- stylesheets -->
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/compiled/theme.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/brankic.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/ionicons.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/font-awesome.min.css" />' />

    <!-- javascript -->
    <script src='<core:url value="/resources/js/vendor/jquery-2.1.1.min.js" />'></script>
    <script src='<core:url value="/resources/js/bootstrap/bootstrap.min.js" />'></script>
    <script src='<core:url value="/resources/js/theme.js" />'></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body id="signin" class="">

<% session.invalidate(); %>

<%--<div class="signup-switcher">--%>
    <%--<a href="#" class="active" data-class="clear">--%>
        <%--<i class="fa fa-check"></i>--%>
        <%--Clear--%>
    <%--</a>--%>
    <%--<a href="#" data-class="">--%>
        <%--<i class="fa fa-check"></i>--%>
        <%--Dark--%>
    <%--</a>--%>
<%--</div>--%>

<a href="#" class="logo">
    <img src='<core:url value="/resources/images/logos/logo-andina.png" />' alt="andina" style="width: 90px" />
    <img src='<core:url value="/resources/images/logos/logo-utc.gif" />' alt="utc" style="width: 90px;" />
</a>

<h3>Sistema de Gestión Documental v1.0</h3>

<div class="content">
    <form name="f" action="j_spring_security_check" method="POST">
        <div class="fields">
            <strong>Usuario</strong>
            <input class="form-control" type="text" placeholder="Nombre de usuario" name="j_username" />
        </div>
        <div class="fields">
            <strong>Contraseña</strong>
            <input class="form-control" type="password" placeholder="Contraseña" name="j_password" />
        </div>
        <%--<div class="info">--%>
            <%--<label>--%>
                <%--<input type="checkbox" name="remember" checked />--%>
                <%--Remember me--%>
            <%--</label>--%>
        <%--</div>--%>
        <div class="actions">
            <button type="submit" class="btn btn-primary btn-lg">Ingresar</button>
        </div>
    </form>
</div>

<div class="bottom-wrapper">
    <%--<div class="message">--%>
        <%--<span>Don't have an account?</span>--%>
        <%--<a href="signup.html">Sign up here</a>.--%>
    <%--</div>--%>
</div>

<%--<script type="text/javascript">--%>
    <%--$(function () {--%>
        <%--var $switcher = $(".signup-switcher a");--%>
        <%--$switcher.click(function (e) {--%>
            <%--e.preventDefault();--%>
            <%--$switcher.removeClass("active");--%>
            <%--$(this).addClass("active");--%>
            <%--$("body").attr("class", $(this).data("class"));--%>
        <%--});--%>
    <%--});--%>
<%--</script>--%>
</body>
</html>
