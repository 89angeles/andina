<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-template" class="form-horizontal" method="post" action="/mail-sender/send" role="form">
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Email</label>
                <div class="col-sm-10 col-md-8">
                    <input type="email" class="form-control" placeholder="Email" name="email" value=""
                           data-rule-required="true" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Asunto</label>
                <div class="col-sm-10 col-md-8">
                    <input type="text" class="form-control" placeholder="Asunto" name="subject" value=""
                           data-rule-required="true" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Mensage</label>
                <div class="col-sm-10 col-md-8">
                    <textarea class="form-control" rows="7" name="message" data-rule-required="true"></textarea>
                </div>
            </div>
            <tag:form-actions url="/templates" send="true" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>