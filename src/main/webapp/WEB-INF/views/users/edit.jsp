    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-customer" class="form-horizontal" method="post" action="/users/save" role="form">
            <form:hidden path="user.id" />
            <tag:input path="user.identificationNumber" label="C&eacute;dula de Identidad"
                       required="true"
                       identity="true" />
            <tag:input path="user.username"  label="Usuario"
                       required="true"
                       minlength="3"
                       alpha="true" />
            <tag:input path="user.firstName" label="Nombres"
                       required="true"
                       minlength="3"
                       alpha="true"
                       cssClass="form-control name" />
            <tag:input path="user.lastName"  label="Apellidos"
                       required="true"
                       minlength="3"
                       alpha="true"
                       cssClass="form-control name" />
            <tag:password path="user.password"  label="Contrase&ntilde;a" />
            <tag:select path="user.role" items="${roles}" label="Perfil" />
            <tag:select path="user.status" items="${statuses}" label="Estado" />
            <tag:form-actions url="/users" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>
