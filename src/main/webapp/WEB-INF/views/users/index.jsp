<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="default">
    <tiles:putAttribute name="body">
        <security:authorize access="isAuthenticated()" />
        <table id="datatable" class="table table-hover">
            <thead>
            <tr>
                <th>Nombres</th>
                <th>C&eacute;dula</th>
                <th>Usuario</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <core:forEach var="user" items="${users}">
                <tr>
                    <td>
                        <span class="name">${user.firstName} ${user.lastName}</span>
                        <tag:role role="${user.role}" />
                    </td>
                    <td>${user.identificationNumber}</td>
                    <td>${user.username}</td>
                    <td><tag:status status="${user.status}" /></td>
                    <td style="width: 10%;">
                        <tag:actions-button baseUrl="/users" id="${user.id}" />
                    </td>
                </tr>
            </core:forEach>
        </table>
        <tag:confirm-modal />
    </tiles:putAttribute>
</tiles:insertDefinition>
