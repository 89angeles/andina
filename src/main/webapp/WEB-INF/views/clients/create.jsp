<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-customer" class="form-horizontal" method="post" action="/clients/save" role="form">
            <form:hidden path="client.id" />
            <tag:input path="client.identityCard"  label="No. de C&eacute;dula"
                       required="true"
                       identity="true" />
            <tag:input path="client.firstName"  label="Nombres"
                       required="true"
                       minlength="3"
                       alpha="true"
                       cssClass="form-control name"/>
            <tag:input path="client.lastName"  label="Apellidos"
                       required="true"
                       minlength="3"
                       alpha="true"
                       cssClass="form-control name"/>
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Email</label>
                <div class="col-sm-10 col-md-8">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="${client.email}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Direcci&oacute;n</label>
                <div class="col-sm-10 col-md-8">
                    <input type="text" class="form-control" placeholder="Direcci&oacute;n" name="address" value="${client.address.address}"
                            data-rule-required="true"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-md-4 col-sm-offset-2 col-lg-offset-2">
                    <form:select path="client.address.city"
                                 cssClass="form-control"
                                 required="required"
                                 items="${cities}"
                                 data-smart-select="data-smart-select" />
                </div>
                <div class="col-sm-5 col-md-4">
                    <input type="text" class="form-control" placeholder="Barrio" name="neighborhood" value="${client.address.neighborhood}"
                           data-rule-alpha="true" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-5 col-md-4 col-sm-offset-2 col-lg-offset-2">
                    <input type="text" class="form-control mobile-margin-bottom" placeholder="Provincia" name="province" value="${client.address.province}"
                           data-rule-alpha="true" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-md-8 col-sm-offset-2 col-lg-offset-2">
                    <input type="text" class="form-control mobile-margin-bottom" placeholder="Referencia" name="reference" value="${client.address.reference}"
                            data-rule-required="true"/>
                </div>
            </div>
            <tag:select path="client.status" items="${statuses}" label="Estado" />
            <tag:form-actions url="/clients" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>
