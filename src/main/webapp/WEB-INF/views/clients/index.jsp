<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="default">
    <tiles:putAttribute name="body">
        <security:authorize access="isAuthenticated()" />
        <table id="datatable" class="table table-hover">
            <thead>
            <tr>
                <th>Nombres</th>
                <th>C&eacute;dula de Identidad</th>
                <th>F. Creaci&oacute;n</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <core:forEach var="client" items="${clients}">
                <tr>
                    <td>${client.fullName}</td>
                    <td>${client.identityCard}</td>
                    <td>${client.creationDate}</td>
                    <td><tag:status status="${client.status}" /></td>
                    <td style="width: 10%;">
                        <tag:actions-button baseUrl="/clients" id="${client.id}" />
                    </td>
                </tr>
            </core:forEach>
        </table>
        <tag:confirm-modal />
    </tiles:putAttribute>
</tiles:insertDefinition>
