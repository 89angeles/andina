<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>

<!-- Imports -->
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>

<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${title}</title>

    <!-- stylesheets -->
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/compiled/theme.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/ionicons.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/brankic.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/font-awesome.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/jquery.dataTables.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/jquery.gritter.css" />' />

    <!-- javascript -->
    <script src='<core:url value="/resources/js/vendor/jquery-2.1.1.min.js" />'></script>
    <script src='<core:url value="/resources/js/bootstrap/bootstrap.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.dataTables.min.js" />'></script>
    <!-- Added localization support. -->
    <script src='<core:url value="/resources/js/vendor/localization/dataTable.spanish.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.gritter.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.cookie.js" />'></script>
    <script src='<core:url value="/resources/js/theme.js" />'></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body id="users">
    <%--<tiles:insertAttribute name="header" />--%>
    <div class="wrapper">
        <div id="sidebar-default" class="main-sidebar">
            <tiles:insertAttribute name="currentUser" />
            <tiles:insertAttribute name="menu" />
            <div class="bottom-menu hidden-sm">
            </div>
        </div>
        <%-- Content --%>
        <div id="content">
            <div class="menubar fixed">
                <div class="sidebar-toggler visible-xs">
                    <i class="ion-navicon"></i>
                </div>
                <div class="page-title">
                    ${title}
                </div>
                <form class="search hidden-xs">
                    <i class="fa fa-search"></i>
                    <input type="text" id="search" name="search" placeholder="${searchText}" />
                </form>
                <core:if test="${not empty create}">
                    <a href="${create}" class="new-user btn btn-success pull-right">
                        <span>${buttonText}</span>
                    </a>
                </core:if>
            </div>
            <div class="content-wrapper">
                <tiles:insertAttribute name="body" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $('#datatable').dataTable({
                "sPaginationType": "full_numbers",
                "iDisplayLength": 20,
                "aLengthMenu": [[20, 50, 100, -1], [20, 50, 100, "Todos"]],
                "bStateSave": true,
                "oLanguage": spanish,
                "aoColumnDefs": [
                    {
                        "bSortable": false,
                        "aTargets": [ -1 ]
                    }
                ]
            });

            <core:if test="${not empty flashMessage}">
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Información',
                // (string | mandatory) the text inside the notification
                text: '${flashMessage}',
                // (bool | optional) if you want it to fade out on its own or just sit there
                sticky: false
            });

            </core:if>

            $('#datatable_filter').hide();
            var searchInput = $('#datatable_filter').find('label').find('input');

            $('#search').on('keyup', function() {
                searchInput.val($(this).val());
                searchInput.trigger('keyup');
            });

            $("#confirm-modal").on('show.bs.modal', function(e) {
                $(this).find('.danger').attr('href', $(e.relatedTarget).data('href'));
                console.log(':(');
                console.log($(e.relatedTarget).data('href'));
            });

        });

    </script>
</body>
</html>
