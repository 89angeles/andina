<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<!-- Imports -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>

<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>${title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- stylesheets -->
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/compiled/theme.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/animate.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/brankic.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/ionicons.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/font-awesome.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/jquery.gritter.css" />' />

    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/select2.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/select2-bootstrap.css" />' />

    <!-- javascript -->
    <script src='<core:url value="/resources/js/vendor/jquery-2.1.1.min.js" />'></script>
    <script src='<core:url value="/resources/js/bootstrap/bootstrap.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/select2.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.validate.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/localization/messages_es.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.gritter.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.cookie.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.maskedinput.js" />'></script>
    <script src='<core:url value="/resources/js/theme.js" />'></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body id="form">
    <div id="wrapper">
        <div id="sidebar-default" class="main-sidebar">
            <tiles:insertAttribute name="currentUser" />
            <tiles:insertAttribute name="menu" />
            <%-- bottom-menu --%>
        </div>

        <div id="content">
            <div class="menubar">
                <div class="sidebar-toggler visible-xs">
                    <i class="ion-navicon"></i>
                </div>

                <div class="page-title">
                    ${title}
                </div>
            </div>

            <div class="content-wrapper">
                <tiles:insertAttribute name="body" />
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('form').validate({
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('success').addClass('error');
                },
                success: function (element) {
                    element.addClass('valid').closest('.form-group').removeClass('error').addClass('success');
                }
            });
            getSelectedRequisites();

            $('.requisite-option').on('change', function () {
                getSelectedRequisites();
            });
        });

        function getSelectedRequisites() {
            var requirements = [];
            $('.requirements-list input:checked').each(function(index, option) {
                requirements.push($(option).val());
            });

            $('#requirements').val(requirements);
        }

        <core:if test="${not empty flashMessage}">
        $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Información',
            // (string | mandatory) the text inside the notification
            text: '${flashMessage}',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: false
        });

        </core:if>
    </script>
</body>
</html>