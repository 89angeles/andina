<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<!-- Imports -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>

<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>${title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- stylesheets -->
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/compiled/theme.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/brankic.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/ionicons.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/vendor/font-awesome.min.css" />' />
    <link rel="stylesheet" type="text/css" href='<core:url value="/resources/css/dropzone.css" />' />

    <!-- javascript -->
    <script src='<core:url value="/resources/js/vendor/jquery-2.1.1.min.js" />'></script>
    <script src='<core:url value="/resources/js/bootstrap/bootstrap.min.js" />'></script>
    <script src='<core:url value="/resources/js/dropzone.min.js" />'></script>
    <script src='<core:url value="/resources/js/vendor/jquery.cookie.js" />'></script>
    <script src='<core:url value="/resources/js/theme.js" />'></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body id="account">
<div id="wrapper">
    <div id="sidebar-default" class="main-sidebar">
        <tiles:insertAttribute name="currentUser" />
        <tiles:insertAttribute name="menu" />
        <%-- bottom-menu --%>
    </div>

<div id="content">

    <div id="sidebar">
        <div class="sidebar-toggler visible-xs">
            <i class="ion-navicon"></i>
        </div>

        <h3>Detalles</h3>
        <ul class="menu">
            <li>
                <a href="#" class="active">
                    <i class="iion-briefcase"></i>
                    Documentos
                </a>
            </li>
        </ul>
    </div>

    <tiles:insertAttribute name="body" />

</div>
</div>

</body>
</html>
