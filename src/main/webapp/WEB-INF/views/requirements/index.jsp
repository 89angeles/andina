<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="default">
    <tiles:putAttribute name="body">
        <security:authorize access="isAuthenticated()" />
        <table id="datatable" class="table table-hover">
            <thead>
            <tr>
                <th>Requisito</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <core:forEach var="requirement" items="${requirements}">
                <tr>
                    <td>${requirement.name}</td>
                    <td><tag:status status="${requirement.status}" /></td>
                    <td style="width: 10%;">
                        <tag:actions-button baseUrl="/requirements" id="${requirement.id}" />
                    </td>
                </tr>
            </core:forEach>
        </table>
        <tag:confirm-modal />
    </tiles:putAttribute>
</tiles:insertDefinition>
