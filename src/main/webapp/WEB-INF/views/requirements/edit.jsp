    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-customer" class="form-horizontal" method="post" action="/requirements/save" role="form">
            <form:hidden path="requirement.id" />
            <tag:input path="requirement.name" label="Requerimiento"
                       required="true"
                       minlength="3"
                       alpha="true" />
            <tag:input path="requirement.description" label="Descripci&oacute;n"
                       minlength="3"
                       alpha="true" />
            <tag:select path="requirement.status" items="${statuses}" label="Estado" />
            <tag:form-actions url="/requirements" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>
