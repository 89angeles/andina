<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="default">
    <tiles:putAttribute name="body">
        <security:authorize access="isAuthenticated()" />
        <table id="datatable" class="table table-hover">
            <thead>
            <tr>
                <th># de Operaci&oacute;n</th>
                <th>Cr&eacute;dito</th>
                <th>Socio</th>
                <th>C&eacute;dula del Socio</th>
                <th>F. Creaci&oacute;n</th>
                <th>Estado</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <core:forEach var="operation" items="${operations}">
                <tr>
                    <td><a href="/operations/${operation.id}/details">${operation.referenceNumber}</a></td>
                    <td>${operation.credit.name}</td>
                    <td>${operation.client.fullName}</td>
                    <td>${operation.client.identityCard}</td>
                    <td>${operation.creationDate}</td>
                    <td><tag:status status="${operation.status}" /></td>
                    <td style="width: 10%;">
                        <tag:actions-button baseUrl="/operations" id="${operation.id}" />
                    </td>
                </tr>
            </core:forEach>
        </table>
        <tag:confirm-modal />
    </tiles:putAttribute>
</tiles:insertDefinition>
