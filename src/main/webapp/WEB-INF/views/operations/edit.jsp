<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-customer" class="form-horizontal" method="post" action="/operations/save" role="form">
            <form:hidden path="operation.id" />
            <tag:input path="operation.referenceNumber" label="No. de Cr&eacute;dito" required="true" number="true" />
            <tag:select path="operation.credit" items="${credits}" label="Tipo de Cr&eacute;dito" />
            <tag:select path="operation.client" items="${clients}" label="Socio" />
            <tag:select path="operation.status" items="${statuses}" label="Estado" />
            <tag:form-actions url="/operations" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>
