<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="details">
    <tiles:putAttribute name="body">
        <div id="panel" class="billing">
            <h3>
                Operaci&oacute;n # ${operation.referenceNumber}
            </h3>

            <div class="plan">

                <div class="current-plan">
                    <div class="field">
                        <label>Tipo de Cr&eacutedito:</label> ${operation.credit.name}
                    </div>

                    <div class="field">
                        <label>Beneficiario:</label> ${operation.client.fullName}
                    </div>
                    <div class="field">
                        <label>&nbsp;</label> <strong>${operation.client.identityCard}</strong>
                    </div>

                    <div class="field">
                        <label>Fecha de Apertura:</label> ${operation.creationDate}
                    </div>

                    <div class="field status">
                        <label>Estado:</label> <tag:field-status status="${operation.status}" />
                    </div>
                </div>

                <div class="current-cc">
                    <label>Oficial de Cr&eacute;dito:</label>
                    ${operation.creator.fullName}
                    <span class="next">
                        <a class="btn btn-sm btn-danger suspend-sub" href="/operations/${operation.id}/details">Regresar</a>
                    </span>
                </div>

                <div class="invoices">
                    <form action="/upload" class="dropzone" id="dropzone">
                        <form:hidden path="operation.id" />
                        <input id="documentId" type="hidden" value="${document}" name="documentId" />
                    </form>
                </div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
