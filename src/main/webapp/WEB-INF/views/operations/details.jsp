<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="details">
    <tiles:putAttribute name="body">
        <div id="panel" class="billing">
            <h3>
                Operaci&oacute;n # ${operation.referenceNumber}
            </h3>

            <div class="plan">

                <div class="current-plan">
                    <div class="field">
                        <input type="hidden" id="operation-id" value="${operation.id}" />
                        <label>Tipo de Cr&eacutedito:</label> ${operation.credit.name}
                    </div>

                    <div class="field">
                        <label>Beneficiario:</label> ${operation.client.fullName}
                    </div>
                    <div class="field">
                        <label>&nbsp;</label> <strong>${operation.client.identityCard}</strong>
                    </div>

                    <div class="field">
                        <label>Fecha de Apertura:</label> ${operation.creationDate}
                    </div>

                    <div class="field status">
                        <label>Estado:</label> <tag:field-status status="${operation.status}" />
                    </div>
                </div>

                <div class="current-cc">
                    <label>Oficial de Cr&eacute;dito:</label>
                    ${operation.creator.fullName}
                    <span class="next">
                        <a class="btn btn-sm btn-danger suspend-sub" href="/operations">Operaciones</a>
                        <core:if test="${ not empty operation.client.email}">
                            <a class="btn btn-sm btn-success suspend-sub send-documents">Notificar Cliente</a>
                            <span class="checked-mail" style="display: none;">
                                <i class="ion-checkmark-circled"></i>
                            </span>
                            <div id="loading-indicator" style="display: none;">
                                Enviando ...
                            </div>
                        </core:if>
                    </span>
                </div>


                <div class="invoices">
                    <h3>Documentos</h3>

                    <table id="documents" class="table">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                Documento
                            </th>
                            <th>
                                Fecha de Carga
                            </th>
                            <th>
                                Cargado por
                            </th>
                            <th>
                                Nombre de Archivo
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                        <core:forEach var="document" items="${operation.documents}">
                            <tr>
                                <td>
                                    <core:if test="${ not empty document.file.id}">
                                        <span class="checked-document">
                                            <i class="ion-checkmark-circled"></i>
                                        </span>
                                    </core:if>
                                </td>
                                <td>
                                    <a href="/operations/${operation.id}/${document.id}/load">${document.name}</a>
                                </td>
                                <td>
                                    ${document.uploadDate}
                                </td>
                                <td>
                                    ${document.uploader}
                                </td>
                                <td>
                                    ${document.file.name}
                                </td>
                                <td>
                                    <core:if test="${ not empty document.file.id}">
                                        <a class="button" href="/download/${document.file.id}">
                                            <span>Descargar</span>
                                        </a>
                                    </core:if>
                                </td>
                            </tr>
                        </core:forEach>
                    </table>
                </div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
