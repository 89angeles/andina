<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:insertDefinition name="form">
    <tiles:putAttribute name="body">
        <form id="new-customer" class="form-horizontal" method="post" action="/credits/save" role="form">
            <input type="hidden" value="" id="requirements" name="requirements" />
            <form:hidden path="credit.id" />
            <tag:input path="credit.name"  label="Cr&eacute;dito"
                       required="true"
                       minlength="3"
                       alpha="true" />
            <tag:input path="credit.description" label="Descripci&oacute;n"
                       minlength="3"
                       alpha="true" />
            <tag:select path="credit.status" items="${statuses}" label="Estado" />
            <div class="form-group">
                <label class="col-sm-2 col-lg-2 control-label">Requisitos</label>
                <div class="col-sm-10 col-md-8">
                    <div class="requirements-list">
                        <core:forEach var="requirement" items="${requirements}">
                            <div class="row requirement">
                                <div class="col-sm-1 col-lg-1">
                                    <input type="checkbox" class="requisite-option" value="${requirement.id}" ${requirement.selected ? 'checked' : ''} />
                                </div>
                                <div class="col-sm-8 col-lg-6">
                                        ${requirement.name}
                                </div>
                            </div>
                        </core:forEach>
                    </div>
                </div>
            </div>
            <tag:form-actions url="/credits" />
        </form>
    </tiles:putAttribute>
</tiles:insertDefinition>
