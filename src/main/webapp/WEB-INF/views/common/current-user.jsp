<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="current-user">
    <a href="index.html" class="name">
        <%--<img class="avatar" src='<core:url value="/resources/images/avatars/1.jpg" />' />--%>
        <span>
            <core:if test="${not empty currentUser}">
                <core:out value="${currentUser.fullName}" /><br />
                <tag:role-header role="${currentUser.role}" />
            </core:if>
            <i class="fa fa-chevron-down"></i>
        </span>
    </a>
    <ul class="menu">
        <li>
            <a href="/j_spring_security_logout">Salir</a>
        </li>
    </ul>
</div>
