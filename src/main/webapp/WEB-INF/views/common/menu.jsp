<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>


<security:authorize access="hasRole('ROLE_ADMINISTRATOR')">
    <div class="menu-section">
        <h3>Administrador</h3>
        <ul>
            <li>
                <a href="/users">
                    <i class="ion-person-stalker"></i>
                    <span>Usuarios</span>
                </a>
            </li>
            <li>
                <a href="/mail-sender/compose">
                    <i class="ion-android-mail"></i>
                    <span>Enviar Correo</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="menu-section">
        <h3>Oficial de Cr&eacute;dito</h3>
        <ul>
            <li>
                <a href="/operations">
                    <i class="ion-briefcase"></i>
                    <span>Operaciones</span>
                </a>
            </li>
            <li>
                <a href="/clients">
                    <i class="ion-person-add"></i>
                    <span>Clientes</span>
                </a>
            </li>
            <li>
                <a href="/emails">
                    <i class="ion-archive"></i>
                    <span>Correos Enviados</span>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="sidebar">
                    <i class="ion-gear-b"></i><span>Configuraci&oacute;n</span>
                    <i class="fa fa-chevron-down"></i>
                </a>
                <ul class="submenu">
                    <li><a href="/requirements">Requerimientos</a></li>
                    <li><a href="/credits">Cr&eacute;ditos</a></li>
                    <li><a href="/templates">Plantillas</a></li>
                </ul>
            </li>
            <li>
                <a href='<core:url value="/resources/documents/manual-usuario.pdf" />' target="_blank">
                    <i class="ion-help"></i>
                    <span>Ayuda</span>
                </a>
            </li>
        </ul>
    </div>
</security:authorize>

<security:authorize access="hasRole('ROLE_LOAN_OFFICER')">
    <div class="menu-section">
        <h3>Oficial de Cr&eacute;dito</h3>
        <ul>
            <li>
                <a href="/operations">
                    <i class="ion-briefcase"></i>
                    <span>Operaciones</span>
                </a>
            </li>
            <li>
                <a href='<core:url value="/resources/documents/manual-usuario.pdf" />' target="_blank">
                    <i class="ion-help"></i>
                    <span>Ayuda</span>
                </a>
            </li>
        </ul>
    </div>
</security:authorize>