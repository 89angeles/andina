
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="default">
    <tiles:putAttribute name="body">
        <security:authorize access="isAuthenticated()" />
        <table id="datatable" class="table table-hover">
            <thead>
            <tr>
                <th>Asunto</th>
                <th>Mensaje</th>
                <th>Destinatario</th>
                <th>Fecha</th>
            </tr>
            </thead>
            <core:forEach var="email" items="${emails}">
                <tr>
                    <td>${email.trimmedSubject}</td>
                    <td>${email.trimmedBody}</td>
                    <td>${email.toAddress}</td>
                    <td>${email.createdAt}</td>
                </tr>
            </core:forEach>
        </table>
        <tag:confirm-modal />
    </tiles:putAttribute>
</tiles:insertDefinition>