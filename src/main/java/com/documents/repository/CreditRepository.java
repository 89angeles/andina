package com.documents.repository;

import com.documents.domain.document.Credit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditRepository  extends MongoRepository<Credit, String> {

    public List<Credit> findByStatus(int status);
}
