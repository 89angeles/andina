package com.documents.repository;

import com.documents.domain.document.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends MongoRepository<User, String>,
        PagingAndSortingRepository<User, String> {

    public User findByIdentificationNumber(String identificationNumber);

    public User findByUsername(String username);
}
