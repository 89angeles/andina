package com.documents.repository;

import com.documents.domain.document.Requirement;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequirementRepository extends MongoRepository<Requirement, String> {
}
