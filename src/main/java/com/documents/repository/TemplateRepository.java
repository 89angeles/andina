package com.documents.repository;

import com.documents.domain.document.Template;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends MongoRepository<Template, String>,
        PagingAndSortingRepository<Template, String> {

    public Template findByType(String type);
}
