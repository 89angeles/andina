package com.documents.repository;

import com.documents.domain.Document;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DocumentRepository  extends MongoRepository<Document, String> {
}
