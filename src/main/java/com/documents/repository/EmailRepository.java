package com.documents.repository;

import com.documents.domain.document.Email;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailRepository extends MongoRepository<Email, String>,
        PagingAndSortingRepository<Email, String> {

}
