package com.documents.web;

import com.documents.domain.Address;
import com.documents.domain.document.Client;
import com.documents.service.*;
import com.documents.web.model.ui.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.LinkedList;
import java.util.List;

@Controller
public class ClientsController extends BaseController {
    @Autowired
    private ClientService clientService;

    @Autowired
    private AuthService authService;

    @Autowired
    private CatalogService catalogService;

    private final String BASE_URL = "/clients";
    private final String VIEW_ROOT = "clients";
    private final String RESOURCE = "client";
    private final String COLLECTION = "clients";


    @RequestMapping(value = BASE_URL, method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Clientes");
        model.addAttribute(SEARCH_TEXT, "Buscar cliente");
        model.addAttribute(BUTTON_TEXT, "Nuevo Cliente");
        model.addAttribute(COLLECTION, clientService.findAll());
        model.addAttribute("create", BASE_URL + "/create");

        return VIEW_ROOT + "/index";
    }

    @RequestMapping(value = BASE_URL +"/create", method = RequestMethod.GET)
    public String Create(ModelMap model) {

        Client client = new Client();
        model.addAttribute("title", "Nuevo Cliente");
        model.addAttribute(RESOURCE, client);
        model.addAttribute("cities", catalogService.getCities());
        model.addAttribute("statuses", catalogService.getStatuses());

        return VIEW_ROOT + "/create";
    }

    @RequestMapping(value = BASE_URL + "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String id, ModelMap model) {
        Client client = clientService.findById(id);
        model.addAttribute("title", "Editar Crédito");
        model.addAttribute(RESOURCE, client);
        model.addAttribute("cities", catalogService.getCities());
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("requirements", getSelectedRequirements(client));

        return VIEW_ROOT + "/edit";
    }

    @RequestMapping(value = BASE_URL + "/save", method = RequestMethod.POST)
    public View save(
            @RequestParam("id") String id,
            @RequestParam("identityCard") String identityCard,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("email") String email,
            @RequestParam("status") String status,
            @RequestParam("address") String address,
            @RequestParam("address.city") String city,
            @RequestParam("neighborhood") String neighborhood,
            @RequestParam("province") String province,
            @RequestParam("reference") String reference,
            final RedirectAttributes redirectAttributes) {

        // Check uniqueness for client on creation operation.
        if (id == "") {
            if (clientService.clientExists(identityCard)) {
                redirectAttributes.addFlashAttribute("flashMessage", "La cédula de identidad " + identityCard + " ya fue registrada para otro cliente.");
                return new RedirectView("create");
            }
        }

        Client client = new Client(
                id,
                identityCard,
                firstName,
                lastName,
                email,
                new Address(address, city, neighborhood, province, reference),
                authService.getCurrentUser(),
                Integer.parseInt(status));

        if (StringUtils.hasText(client.getId())) {
            clientService.update(client);
            redirectAttributes.addFlashAttribute("flashMessage", "Cliente modificado correctamente.");
        } else {
            clientService.create(client);
            redirectAttributes.addFlashAttribute("flashMessage", "Cliente creado correctamente.");
        }

        return new RedirectView(BASE_URL);
    }

    @RequestMapping(value = BASE_URL + "/{id}/delete", method = RequestMethod.GET)
    public View delete(@PathVariable String id, final RedirectAttributes redirectAttributes) {
        clientService.delete(id);
        redirectAttributes.addFlashAttribute("flashMessage", "Usuario eliminado correctamente.");
        return new RedirectView(BASE_URL);
    }

    private List<Requirement> getSelectedRequirements(Client client) {
        List<Requirement> requirements = new LinkedList<Requirement>();
        // List<String> selectedRequirements = client.getRequirementsKeys();
//        for(Requirement requirement : requirementService.findAll()) {
//            if (selectedRequirements != null && selectedRequirements.contains(requirement.getId())) {
//                requirements.add(new RequirementUI(requirement, true));
//            } else {
//                requirements.add(new RequirementUI(requirement, false));
//            }
//        }

        return requirements;
    }
}
