package com.documents.web;

import com.documents.domain.document.Operation;
import com.documents.domain.document.Template;
import com.documents.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class TemplatesController extends BaseController {

    @Autowired
    private TemplateService templateService;

    private final String BASE_URL = "/templates";
    private final String VIEW_ROOT = "templates";

    @RequestMapping(value=BASE_URL, method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Plantillas");
        model.addAttribute("templates", templateService.findAll());

        return VIEW_ROOT + "/index";
    }

    @RequestMapping(value = BASE_URL + "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String id, ModelMap model) {
        model.addAttribute("title", "Editar Plantilla");
        model.addAttribute("template", templateService.findById(id));

        return VIEW_ROOT + "/edit";
    }

    @RequestMapping(value = BASE_URL + "/save", method = RequestMethod.POST)
    public View save(
            @RequestParam("id") String id,
            @RequestParam("name") String name,
            @RequestParam("subject") String subject,
            @RequestParam("message") String message,
            final RedirectAttributes redirectAttributes) {

        Template template = new Template(id, name, subject, message);

        if (templateService.update(template) != null) {
            redirectAttributes.addFlashAttribute("flashMessage", "Plantilla modificada correctamente.");
        }

        return new RedirectView(BASE_URL);
    }
}
