package com.documents.web;


import com.documents.domain.document.Client;
import com.documents.domain.document.Operation;
import com.documents.domain.document.Template;
import com.documents.service.MailSenderService;
import com.documents.service.OperationService;
import com.mongodb.util.JSON;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class MailSenderController {

    @Autowired
    private MailSenderService mailSenderService;

    @Autowired
    private OperationService operationService;

    private final String BASE_URL = "/mail-sender";
    private final String VIEW_ROOT = "mail-sender";


    @RequestMapping(value = BASE_URL +"/compose", method = RequestMethod.GET)
    public String compose() {

        return VIEW_ROOT + "/compose";
    }

    @RequestMapping(value = BASE_URL + "/send", method = RequestMethod.POST)
    public View send(
            @RequestParam("email") String email,
            @RequestParam("subject") String subject,
            @RequestParam("message") String message,
            final RedirectAttributes redirectAttributes) {

        if (mailSenderService.send(email, subject, message)) {
            redirectAttributes.addFlashAttribute("flashMessage", "Mensaje enviado correctamente.");
        }

        return new RedirectView("/emails");
    }

    @RequestMapping(value = BASE_URL,
            method = RequestMethod.GET,
            produces = "application/json")
    public @ResponseBody ResponseEntity<String> send(@RequestParam("operationId") String operationId) {
        Operation operation = operationService.findById(operationId);

        if (mailSenderService.send(operation)) {

            // Use ResponseEntity instead of String
            // http://stackoverflow.com/q/16232833/
            return new ResponseEntity<String>("\"Success\"", HttpStatus.OK);
        }

        return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
