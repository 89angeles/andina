package com.documents.web;

import com.documents.domain.document.Operation;
import com.documents.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class OperationsController extends BaseController {
    @Autowired
    private OperationService operationService;

    @Autowired
    private AuthService authService;

    @Autowired
    private CatalogService catalogService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private CreditService creditService;

    private final String BASE_URL = "/operations";
    private final String VIEW_ROOT = "operations";
    private final String RESOURCE = "operation";
    private final String COLLECTION = "operations";


    @RequestMapping(value = BASE_URL, method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Operaciones");
        model.addAttribute(SEARCH_TEXT, "Buscar operación");
        model.addAttribute(BUTTON_TEXT, "Nueva Operación");
        model.addAttribute(COLLECTION, operationService.findAll());
        model.addAttribute("create", BASE_URL + "/create");
        return VIEW_ROOT + "/index";
    }

    @RequestMapping(value = BASE_URL +"/create", method = RequestMethod.GET)
    public String create(ModelMap model) {

        Operation operation = new Operation();
        model.addAttribute("title", "Nueva Operación");
        model.addAttribute(RESOURCE, operation);
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("clients", catalogService.getClients());
        model.addAttribute("credits", catalogService.getCredits());

        return VIEW_ROOT + "/create";
    }

    @RequestMapping(value = BASE_URL + "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String id, ModelMap model) {
        Operation operation = operationService.findById(id);
        model.addAttribute("title", "Editar Operación");
        model.addAttribute(RESOURCE, operation);
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("clients", catalogService.getClients());
        model.addAttribute("credits", catalogService.getCredits());

        return VIEW_ROOT + "/edit";
    }

    @RequestMapping(value = BASE_URL + "/{id}/details", method = RequestMethod.GET)
    public String details(@PathVariable String id, ModelMap model) {
        Operation operation = operationService.findById(id);
        model.addAttribute("title", "Editar Operación");
        model.addAttribute(RESOURCE, operation);

        return VIEW_ROOT + "/details";
    }

    @RequestMapping(value = BASE_URL + "/save", method = RequestMethod.POST)
    public View save(
            @RequestParam("id") String id,
            @RequestParam("referenceNumber") String referenceNumber,
            @RequestParam("credit") String credit,
            @RequestParam("client") String client,
            @RequestParam("status") String status,
            final RedirectAttributes redirectAttributes) {

        Operation operation = new Operation(id,
                referenceNumber,
                creditService.findById(credit),
                clientService.findById(client),
                authService.getCurrentUser(),
                Integer.parseInt(status));

        if (StringUtils.hasText(operation.getId())) {
            operationService.update(operation);
            redirectAttributes.addFlashAttribute("flashMessage", "Operación modificada correctamente.");
        } else {
            operationService.create(operation);
            redirectAttributes.addFlashAttribute("flashMessage", "Operación creada correctamente.");
        }
        return new RedirectView(BASE_URL);
    }

    @RequestMapping(value = BASE_URL + "/{id}/delete", method = RequestMethod.GET)
    public View delete(@PathVariable String id, final RedirectAttributes redirectAttributes) {
        operationService.delete(id);
        redirectAttributes.addFlashAttribute("flashMessage", "Operación eliminada correctamente.");
        return new RedirectView(BASE_URL);
    }

    @RequestMapping(value = BASE_URL + "/{id}/{document}/load", method = RequestMethod.GET)
    public String load(@PathVariable String id, @PathVariable String document, ModelMap model) {
        Operation operation = operationService.findById(id);


        model.addAttribute(RESOURCE, operation);
        model.addAttribute("document", document);
        model.addAttribute("id", id);
        return VIEW_ROOT + "/load";
    }
}
