package com.documents.web;

import com.documents.domain.document.Requirement;
import com.documents.domain.document.User;
import com.documents.service.AuthService;
import com.documents.service.CatalogService;
import com.documents.service.RequirementService;
import com.documents.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class RequirementsController extends BaseController {

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private CatalogService catalogService;

    private final String BASE_URL = "/requirements";
    private final String VIEW_ROOT = "requirements";

    @RequestMapping(value = BASE_URL, method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Requerimientos");
        model.addAttribute(SEARCH_TEXT, "Buscar requerimiento");
        model.addAttribute(BUTTON_TEXT, "Nuevo requerimiento");
        model.addAttribute("requirements", requirementService.findAll());
        model.addAttribute("create", BASE_URL + "/create");

        return VIEW_ROOT + "/index";
    }

    @RequestMapping(value = BASE_URL +"/create", method = RequestMethod.GET)
    public String Create(ModelMap model) {
        model.addAttribute("title", "Nuevo Requerimiento");
        model.addAttribute("requirement", new Requirement());
        model.addAttribute("statuses", catalogService.getStatuses());

        return VIEW_ROOT + "/create";
    }

    @RequestMapping(value = BASE_URL + "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String id, ModelMap model) {
        model.addAttribute("title", "Editar Requisito");
        model.addAttribute("requirement", requirementService.findById(id));
        model.addAttribute("statuses", catalogService.getStatuses());
        return VIEW_ROOT + "/edit";
    }

    @RequestMapping(value = BASE_URL + "/save", method = RequestMethod.POST)
    public View save(@ModelAttribute Requirement requirement, final RedirectAttributes redirectAttributes) {
        if (StringUtils.hasText(requirement.getId())) {
            requirementService.update(requirement);
            redirectAttributes.addFlashAttribute("flashMessage", "Requisito modificado correctamente.");
        } else {
            requirementService.create(requirement);
            redirectAttributes.addFlashAttribute("flashMessage", "Requisito creado correctamente.");
        }
        return new RedirectView(BASE_URL);
    }

    @RequestMapping(value = BASE_URL + "/{id}/delete", method = RequestMethod.GET)
    public View delete(@PathVariable String id, final RedirectAttributes redirectAttributes) {
        requirementService.delete(id);
        redirectAttributes.addFlashAttribute("flashMessage", "Usuario eliminado correctamente.");
        return new RedirectView(BASE_URL);
    }
}
