package com.documents.web;

public class BaseController {

    protected final static String TITLE = "title";
    protected final static String BUTTON_TEXT = "buttonText";
    protected final static String SEARCH_TEXT = "searchText";
}
