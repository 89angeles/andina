package com.documents.web;

import com.documents.domain.Document;
import com.documents.domain.File;
import com.documents.domain.document.Operation;
import com.documents.service.AuthService;
import com.documents.service.FileService;
import com.documents.service.OperationService;
import com.mongodb.gridfs.GridFSDBFile;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;

// import org.springframework.ui.ModelMap;

@Controller
public class UploadController {

    @Autowired
    private FileService fileService;

    @Autowired
    private OperationService operationService;

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/upload",
            method = RequestMethod.POST,
            produces = "application/json")
    public @ResponseBody ResponseEntity<String> save(
            @RequestParam("file") MultipartFile file,
            @RequestParam("id") String id,
            @RequestParam("documentId") String documentId) {

        try {
            if (!file.isEmpty()) {
                // Backend validation to prevent invalid files to be uploaded.
                String[] extensionsWhitelist = { "jpg", "png", "pdf" };
                String extension = FilenameUtils.getExtension(file.getOriginalFilename());

                if (!Arrays.asList(extensionsWhitelist).contains(extension)) {
                    throw new Exception("File not allowed");
                }

                Operation operation = operationService.findById(id);
                for(Document document : operation.getDocuments()) {
                    if (document.getId().equals(documentId)) {
                        if (document.getFile() != null) {
                            fileService.delete(document.getFile().getId());
                        }

                        String fileId = fileService.save(file);

                        document.setFile(new File(fileId, file.getOriginalFilename(), file.getSize(), file.getContentType()));
                        document.setUploadDate(new Date());
                        document.setUploader(authService.getCurrentUser().getUsername());

                        break;
                    }
                }

                operationService.update(operation);

                return new ResponseEntity<String>("\"Success\"", HttpStatus.OK);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<String>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @RequestMapping(value="/download/{id}")
    public void download(HttpServletResponse response,
                         @PathVariable String id) throws IOException {
        OutputStream outputStream = response.getOutputStream();

        GridFSDBFile file = fileService.retrieve(id);

        response.setHeader("Content-disposition", "attachment; filename=" + URLEncoder.encode(file.getFilename(), "ISO8859-1"));

        FileCopyUtils.copy(file.getInputStream(), outputStream);

        outputStream.flush();
        outputStream.close();
    }
}
