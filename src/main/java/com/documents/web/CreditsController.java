package com.documents.web;

import com.documents.domain.document.Credit;
import com.documents.domain.document.User;
import com.documents.service.*;
import com.documents.web.model.ui.Requirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.LinkedList;
import java.util.List;

@Controller
public class CreditsController extends BaseController {

    @Autowired
    private CreditService creditService;

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private AuthService authService;

    @Autowired
    private CatalogService catalogService;

    private final String BASE_URL = "/credits";
    private final String VIEW_ROOT = "credits";
    private final String RESOURCE = "credit";
    private final String COLLECTION = "credits";


    @RequestMapping(value = BASE_URL, method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Créditos");
        model.addAttribute(SEARCH_TEXT, "Buscar crédito");
        model.addAttribute(BUTTON_TEXT, "Nuevo Crédito");
        model.addAttribute(COLLECTION, creditService.findAll());
        model.addAttribute("create", BASE_URL + "/create");

        return VIEW_ROOT + "/index";
    }

    @RequestMapping(value = BASE_URL +"/create", method = RequestMethod.GET)
    public String Create(ModelMap model) {

        Credit credit = new Credit();
        model.addAttribute("title", "Nuevo Crédito");
        model.addAttribute(RESOURCE, credit);
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("requirements", getSelectedRequirements(credit));

        return VIEW_ROOT + "/create";
    }

    @RequestMapping(value = BASE_URL + "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String id, ModelMap model) {
        Credit credit = creditService.findById(id);
        model.addAttribute("title", "Editar Crédito");
        model.addAttribute(RESOURCE, credit);
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("requirements", getSelectedRequirements(credit));

        return VIEW_ROOT + "/edit";
    }

    @RequestMapping(value = BASE_URL + "/save", method = RequestMethod.POST)
    public View save(
            @RequestParam("id") String id,
            @RequestParam("name") String name,
            @RequestParam("description") String description,
            @RequestParam("status") String status,
            @RequestParam("requirements") String requirements,
            final RedirectAttributes redirectAttributes) {
        Credit credit = new Credit(
                id,
                name,
                description,
                requirementService.findAll(requirements),
                authService.getCurrentUser(),
                Integer.parseInt(status));

        if (StringUtils.hasText(credit.getId())) {
            creditService.update(credit);
            redirectAttributes.addFlashAttribute("flashMessage", "Crédito modificado correctamente.");
        } else {
            creditService.create(credit);
            redirectAttributes.addFlashAttribute("flashMessage", "Crédito creado correctamente.");
        }
        return new RedirectView(BASE_URL);
    }

    @RequestMapping(value = BASE_URL + "/{id}/delete", method = RequestMethod.GET)
    public View delete(@PathVariable String id, final RedirectAttributes redirectAttributes) {
        creditService.delete(id);
        redirectAttributes.addFlashAttribute("flashMessage", "Usuario eliminado correctamente.");
        return new RedirectView(BASE_URL);
    }

    private List<Requirement> getSelectedRequirements(Credit credit) {
        List<Requirement> requirements = new LinkedList<Requirement>();
        List<String> selectedRequirements = credit.getRequirementsKeys();
        for(com.documents.domain.document.Requirement requirement : requirementService.findAll()) {
            if (selectedRequirements != null && selectedRequirements.contains(requirement.getId())) {
                requirements.add(new Requirement(requirement, true));
            } else {
                requirements.add(new Requirement(requirement, false));
            }
        }

        return requirements;
    }
}
