package com.documents.web;

import com.documents.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmailsController extends BaseController {

    @Autowired
    private EmailService emailService;

    private final String BASE_URL = "/emails";
    private final String VIEW_ROOT = "emails";
    private final String COLLECTION = "emails";

    @RequestMapping(value = BASE_URL, method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Enviados");
        model.addAttribute(SEARCH_TEXT, "Buscar correo");
        model.addAttribute(COLLECTION, emailService.findAll());

        return VIEW_ROOT + "/index";
    }
}
