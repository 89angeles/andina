package com.documents.web;

import com.documents.domain.document.User;
import com.documents.service.AuthService;
import com.documents.service.CatalogService;
import com.documents.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class UsersController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private CatalogService catalogService;

//    private final String BASE_URL = "/credits";
//    private final String VIEW_ROOT = "credits";
//    private final String RESOURCE = "credit";
//    private final String COLLECTION = "credits";


    @RequestMapping(value="/users", method = RequestMethod.GET)
    public String index(ModelMap model) {

        model.addAttribute(TITLE, "Usuarios");
        model.addAttribute(SEARCH_TEXT, "Buscar usuario");
        model.addAttribute(BUTTON_TEXT, "Nuevo Usuario");

        model.addAttribute("users", userService.findAll());
        model.addAttribute("create", "/users/create");

        return "users/index";
    }

    @RequestMapping(value = "/users/create", method = RequestMethod.GET)
    public String Create(ModelMap model) {
        model.addAttribute("title", "Nuevo Usuario");
        model.addAttribute("user", new User());
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("roles", catalogService.getRoles());
        return "users/create";
    }

    @RequestMapping(value="/users/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable String id, ModelMap model) {
        model.addAttribute("title", "Editar Usuario");
        model.addAttribute("user", userService.findById(id));
        model.addAttribute("statuses", catalogService.getStatuses());
        model.addAttribute("roles", catalogService.getRoles());
        return "users/edit";
    }

    @RequestMapping(value="/users/save", method = RequestMethod.POST)
    public View save(@ModelAttribute User user, final RedirectAttributes redirectAttributes) {

        // Check uniqueness for client on creation operation.
        if (!StringUtils.hasText(user.getId())) {
            if (userService.userExists(user.getIdentificationNumber())) {
                redirectAttributes.addFlashAttribute("flashMessage", "La cédula de identidad " + user.getId() + " ya fue registrada para otro usuario.");
                return new RedirectView("create");
            }
        }

        if (StringUtils.hasText(user.getId())) {
            redirectAttributes.addFlashAttribute("flashMessage", "Usuario modificado correctamente.");
            userService.update(user);
        } else {
            redirectAttributes.addFlashAttribute("flashMessage", "Usuario creado correctamente.");
            userService.create(user);
        }

        return new RedirectView("/users");
    }

    @RequestMapping(value = "users/{id}/delete", method = RequestMethod.GET)
    public View delete(@PathVariable String id, final RedirectAttributes redirectAttributes) {
        userService.delete(id);
        redirectAttributes.addFlashAttribute("flashMessage", "Usuario eliminado correctamente.");
        return new RedirectView("/users");
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String get(ModelMap model) {
        model.addAttribute("users", userService.findAll());
        return "output";
    }
}
