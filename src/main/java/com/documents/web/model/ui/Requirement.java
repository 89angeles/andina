package com.documents.web.model.ui;

public class Requirement {
    private String id;
    private String name;
    private Boolean selected;

    public Requirement(com.documents.domain.document.Requirement requirement, Boolean selected) {
        this.id = requirement.getId();
        this.name = requirement.getName();
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
