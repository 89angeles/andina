package com.documents.web.interceptor;

import com.documents.domain.document.User;
import com.documents.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;


public class CurrentUserAddingHandlerInterceptor extends HandlerInterceptorAdapter {

    /**
     * The name under which the current user name is added to the model map.
     */
    public static final String CURRENT_USER = "currentUser";


    @Autowired
    private AuthService authService;

    @Override
    public void postHandle(final HttpServletRequest request,
                           final HttpServletResponse response, final Object handler,
                           final ModelAndView modelAndView) throws Exception {

        if (modelAndView != null) {

            User currentUser = authService.getCurrentUser();
            modelAndView.getModelMap().
                    addAttribute(CURRENT_USER,
                            currentUser != null ? currentUser : null);
        }
    }
}
