package com.documents.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class HomeController {

    @RequestMapping(method = RequestMethod.GET)
	public String printWelcome(HttpServletRequest request) {
        if (request.isUserInRole("ROLE_ADMINISTRATOR")) {
            return "redirect:/users";
        } else if (request.isUserInRole("ROLE_SUPPORT")) {
            return "redirect:/users";
        } else if (request.isUserInRole("ROLE_LOAN_OFFICER")) {
            return "redirect:/operations";
        }

        return "";
	}
}