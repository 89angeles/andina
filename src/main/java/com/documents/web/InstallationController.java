package com.documents.web;

import com.documents.domain.document.User;
import com.documents.domain.enumeration.Role;
import com.documents.domain.enumeration.Status;
import com.documents.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InstallationController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/install")
    public String install() {
        if (userService.findByUsername("installer") == null) {
            User user = new User();
            user.setUsername("installer");
            user.setFirstName("");
            user.setLastName("Instalador");
            user.setRole(Integer.parseInt(Role.ADMINISTRATOR.getValue()));
            user.setStatus(Status.ACTIVE.getValue());

            user.setPassword("0000000000");

            userService.create(user);
        }

        return "redirect:/login";
    }
}
