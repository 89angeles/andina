package com.documents.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuthController {
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
        return "auth/login";
    }

    @RequestMapping(value="/accessdenied", method = RequestMethod.GET)
    public String s(ModelMap model) {


        return "auth/login";
    }

    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public String defaultAfterLogin(HttpServletRequest request) {
        if (request.isUserInRole("ROLE_ADMINISTRATOR")) {
            return "redirect:/users";
        } else if (request.isUserInRole("ROLE_SUPPORT")) {
            return "redirect:/users";
        } else if (request.isUserInRole("ROLE_LOAN_OFFICER")) {
            return "redirect:/operations";
        }

        // Define the landing page.

        return "redirect:/";
    }





//    @RequestMapping(value = "/logout", method = RequestMethod.GET)
//    public String logout(ModelMap model) {
//
//        return "auth/login";
//    }
}
