package com.documents.domain.enumeration;

public enum Status {
    ACTIVE(1),
    INACTIVE(2),
    DELETED(3);

    private int value;

    private Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
