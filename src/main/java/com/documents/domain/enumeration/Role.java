package com.documents.domain.enumeration;

public enum Role {
    ADMINISTRATOR("1"),
    LOAN_OFFICER("2"),
    SUPPORT("3"),
    HUMAN_RESOURCES("4");

    private String value;

    private Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
