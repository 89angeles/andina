package com.documents.domain.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Document(collection = "emails")
public class Email {

    @Id
    private String id;

    private String toAddress;
    private String subject;
    private String body;
    private Date createdAt;

    @DBRef
    private Client client;

    public Email(String toAddress, String subject, String body, Client client) {
        this.toAddress = toAddress;
        this.subject = subject;
        this.body = body;
        this.client = client;
    }

    public Email() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public String getSubject() {
        return subject;
    }

    public String getTrimmedSubject() {
        if (this.subject.length() > 25) {
            return this.subject.substring(0, 25) + "...";
        }

        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public String getTrimmedBody() {
        if (this.body.length() > 80) {
            return this.body.substring(0, 80) + "...";
        }

        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreatedAt() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
        return formatter.format(this.createdAt);
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
