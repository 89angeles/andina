package com.documents.domain.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Document(collection = "credits")
public class Credit {
    @Id
    private String id;
    @Indexed
    private String name;
    private String description;
    private List<Requirement> requirements;
    private Date creationDate;
    @DBRef
    private User creator;
    private int status;

    public Credit() {
    }

    public Credit(String id, String name, String description, List<Requirement> requirements, User creator, int status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.requirements  = requirements;
        this.creator = creator;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public List<String> getRequirementsKeys() {
        if (this.requirements == null) {
            return null;
        }

        List<String> requirements = new LinkedList<String>();
        for(Requirement requirement : this.requirements) {
            requirements.add(requirement.getId());
        }

        return requirements;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
