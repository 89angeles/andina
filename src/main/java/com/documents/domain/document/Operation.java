package com.documents.domain.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Document(collection = "operations")
public class Operation {
    @Id
    private String id;
    @Indexed
    private String referenceNumber;
    @DBRef
    private Credit credit;
    @DBRef
    private Client client;
    private Date creationDate;
    private List<com.documents.domain.Document> documents;
    @DBRef
    private User creator;
    private int status;

    public Operation() {
    }

    public Operation(String id, String referenceNumber, Credit credit, Client client, User creator, int status) {
        this.id = id;
        this.referenceNumber = referenceNumber;
        this.credit = credit;
        this.client = client;
        this.creator = creator;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getCreationDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
        return formatter.format(this.creationDate);
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<com.documents.domain.Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<com.documents.domain.Document> documents) {
        this.documents = documents;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
