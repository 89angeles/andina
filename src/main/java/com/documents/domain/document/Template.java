package com.documents.domain.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "templates")
public class Template {

    @Id
    private String id;

    private String type;
    private String name;
    private String subject;
    private String message;
    private Date createdAt;
    private Date updatedAt;

    public Template(String id, String type, String name, String subject, String message) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.subject = subject;
        this.message = message;
    }

    public Template(String id, String name, String subject, String message) {
        this.id = id;
        this.name = name;
        this.subject = subject;
        this.message = message;
    }

    public Template() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
