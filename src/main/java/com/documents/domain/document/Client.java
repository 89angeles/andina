package com.documents.domain.document;

import com.documents.domain.Address;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Document(collection = "clients")
public class Client {
    private String id;
    @Indexed
    private String identityCard;
    private String firstName;
    private String lastName;
    private String email;
    private Address address;
    private Date creationDate;
    @DBRef
    private User creator;
    private int status;

    public Client() {
        this.address = new Address();
    }

    public Client(String id, String identityCard, String firstName, String lastName, String email, Address address, User creator, int status) {
        this.id = id;
        this.identityCard = identityCard;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.creator = creator;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public String getFirstName() {
        if (firstName != null) {
            return firstName.toUpperCase();
        }

        return "";
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName.toUpperCase();
    }

    public String getLastName() {
        if (lastName != null) {
            return lastName.toUpperCase();
        }

        return "";
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.toUpperCase();
    }

    public String getFullName() {
        return this.firstName.toUpperCase() + " " + this.lastName.toUpperCase();
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
        return formatter.format(this.creationDate);
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
