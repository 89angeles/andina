package com.documents.domain;

public class File {
    private String id;
    private String name;
    private long size;
    private String contentType;

    public File(String id, String name, long size, String contentType) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.contentType = contentType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType= contentType;
    }
}
