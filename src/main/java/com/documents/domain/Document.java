package com.documents.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Document {
    private String id;
    private String name;
    private File file;
    private Date uploadDate;
    private String uploader;

    public Document(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getUploadDate() {
        if (this.uploadDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
            return formatter.format(this.uploadDate);
        }

        return "";
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }
}
