package com.documents.domain;

public class Address {

    private String address;
    private String city;
    private String neighborhood;
    private String province;
    private String reference;

    public Address() {
    }

    public Address(String address, String city, String neighborhood, String province, String reference) {
        this.address = address;
        this.city = city;
        this.neighborhood = neighborhood;
        this.province = province;
        this.reference = reference;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
