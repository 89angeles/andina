package com.documents.service;

import com.documents.domain.document.Credit;
import com.documents.domain.document.Requirement;
import com.documents.domain.enumeration.Status;
import com.documents.repository.CreditRepository;
import com.documents.repository.RequirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
public class CreditService {
    @Autowired
    private CreditRepository creditRepository;

    public Credit create(Credit credit) {
        credit.setId(UUID.randomUUID().toString());
        credit.setCreationDate(new Date());

        return creditRepository.save(credit);
    }

    public List<Credit> findAll() {
        return creditRepository.findAll();
    }

    public Credit findById(String id) {
        return creditRepository.findOne(id);
    }

    public Credit update(Credit credit) {
        Credit instance = creditRepository.findOne(credit.getId());
        if (instance == null) {
            return null;
        }

        instance.setName(credit.getName());
        instance.setDescription(credit.getDescription());
        instance.setStatus(credit.getStatus());
        instance.setRequirements(credit.getRequirements());
        instance.setCreator(credit.getCreator());

        return creditRepository.save(instance);
    }

    public Boolean delete(String id) {
        Credit instance = creditRepository.findOne(id);;
        if (instance == null) {
            return false;
        }

        instance.setStatus(Status.DELETED.getValue());
        creditRepository.save(instance);
        return true;
    }
}
