package com.documents.service;

import com.documents.domain.Document;
import com.documents.domain.document.Client;
import com.documents.domain.document.Email;
import com.documents.domain.document.Operation;
import com.documents.domain.document.Template;
import com.google.common.base.Joiner;
import com.sun.deploy.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;

@Service("mailSenderService")
public class MailSenderService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private EmailService emailService;

    public void send() {
        // FIXME: Remove this method when this works
        Client client = new Client();
        client.setId("982efaf3-4c6d-42eb-a284-55851302fb02");
        client.setEmail("agustin.camino@gmail.com");

        mailSender.send(buildMailMessage(client));
    }

    public void send(Client client) {

        // FIXME: Refactor Create support class: MailBuilder e.g. mail = MailBuilder(client);
        mailSender.send(buildMailMessage(client));
    }

    public boolean send(Operation operation) {
        try {
            mailSender.send(buildMailMessage(operation));
        } catch(Exception ex) {
            return false;
        }

        return true;
    }

    public boolean send(String to, String subject, String message) {
       try {
            mailSender.send(buildMailMessage(to, subject, message));
        }
        catch(Exception ex) {
            return false;
        }

        return true;
    }

    private SimpleMailMessage buildMailMessage(Client client) {

        Template template = templateService.findByType("missing-documents");

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(client.getEmail());
        email.setSubject(template.getSubject());
        email.setText(template.getMessage());

        Log(client, email);

        return email;
    }

    private SimpleMailMessage buildMailMessage(Operation operation) {

        Template template = templateService.findByType("missing-documents");

        Client client = operation.getClient();

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(client.getEmail());
        email.setSubject(template.getSubject());

        String message = replacePlaceholdersInMessage(operation.getDocuments(), template);
        email.setText(message);

        Log(client, email);

        return email;
    }

    private String replacePlaceholdersInMessage(List<Document> documents, Template template) {
        String representableMissingDocuments = getRepresentableMissingDocuments(documents);
        return template.getMessage()
                .replaceFirst("\\{\\{missing\\-documents\\}\\}", representableMissingDocuments);
    }

    private String getRepresentableMissingDocuments(List<Document> documents) {
        List<String> missingDocuments = new LinkedList<String>();

        for(Document document : documents) {
            if(document.getFile() == null) {
                missingDocuments.add(document.getName());
            }
        }

        return Joiner.on("\r\n").join(missingDocuments);
    }

    private SimpleMailMessage buildMailMessage(String to, String subject, String message) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(to);
        email.setSubject(subject);
        email.setText(message);

        Log(null, email);

        return email;
    }

    private void Log(Client client, SimpleMailMessage mail) {
        emailService.create(new Email(
            mail.getTo()[0],
            mail.getSubject(),
            mail.getText(),
            client
        ));
    }
}
