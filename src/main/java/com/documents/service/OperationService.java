package com.documents.service;

import com.documents.domain.Document;
import com.documents.domain.document.Operation;
import com.documents.domain.document.Requirement;
import com.documents.domain.enumeration.Status;
import com.documents.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
public class OperationService {

    @Autowired
    private OperationRepository operationRepository;

    public Operation create(Operation operation) {
        operation.setId(UUID.randomUUID().toString());
        operation.setCreationDate(new Date());

        List<Document> documents = new LinkedList<Document>();

        for(Requirement requirement : operation.getCredit().getRequirements()) {
            documents.add(new Document(UUID.randomUUID().toString(), requirement.getName()));
        }

        operation.setDocuments(documents);

        return operationRepository.save(operation);
    }

    public List<Operation> findAll() {
        return operationRepository.findAll();
    }

    public Operation findById(String id) {
        return operationRepository.findOne(id);
    }

    public Operation update(Operation operation) {
        Operation instance = operationRepository.findOne(operation.getId());
        if (instance == null) {
            return null;
        }

        instance.setCredit(operation.getCredit());
        instance.setCreator(operation.getCreator());
        instance.setStatus(operation.getStatus());
        instance.setDocuments(operation.getDocuments());

        return operationRepository.save(instance);
    }

    public Boolean delete(String id) {
        Operation instance = operationRepository.findOne(id);
        if (instance == null) {
            return false;
        }

        instance.setStatus(Status.DELETED.getValue());
        operationRepository.save(instance);
        return true;
    }
}
