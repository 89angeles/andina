package com.documents.service;

import com.documents.domain.document.Email;
import com.documents.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class EmailService {

    @Autowired
    private EmailRepository mailRepository;

    public Email create(Email email) {
        email.setId(UUID.randomUUID().toString());
        email.setCreatedAt(new Date());
        return mailRepository.save(email);
    }

    public List<Email> findAll() {
        return mailRepository.findAll();
    }
}
