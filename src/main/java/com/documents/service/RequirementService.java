package com.documents.service;

import com.documents.domain.document.Requirement;
import com.documents.domain.enumeration.Status;
import com.documents.repository.RequirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RequirementService {

    @Autowired
    private RequirementRepository requirementRepository;

    public Requirement create(Requirement requirement) {
        requirement.setId(UUID.randomUUID().toString());
        requirement.setCreationDate(new Date());

        return requirementRepository.save(requirement);
    }

    public List<Requirement> findAll() {
        return requirementRepository.findAll();
    }

    public List<Requirement> findAll(String data) {
        List<Requirement> requirements = new LinkedList<Requirement>();
        data = data.trim();
        if (data.isEmpty()) {
            return null;
        }

        if (data.contains(",")) {
            String[] keys = data.split(",");
            for(Requirement requirement : requirementRepository.findAll(Arrays.asList(keys))) {
                requirements.add(requirement);
            }
        } else {
            requirements.add(requirementRepository.findOne(data));
        }

        return requirements;
    }

    public Requirement findById(String id) {
        return requirementRepository.findOne(id);
    }

    public Requirement update(Requirement requirement) {
        Requirement instance = requirementRepository.findOne(requirement.getId());
         if (instance == null) {
             return null;
         }

        instance.setName(requirement.getName());
        instance.setDescription(requirement.getDescription());
        instance.setStatus(requirement.getStatus());

        return requirementRepository.save(instance);
    }

    public Boolean delete(String id) {
        Requirement instance = requirementRepository.findOne(id);;
        if (instance == null) {
            return false;
        }

        instance.setStatus(Status.DELETED.getValue());
        requirementRepository.save(instance);
        return true;
    }
}
