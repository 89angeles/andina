package com.documents.service;

import com.documents.domain.document.Client;
import com.documents.domain.document.Credit;
import com.documents.domain.enumeration.Role;
import com.documents.domain.enumeration.Status;
import com.documents.repository.ClientRepository;
import com.documents.repository.CreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.HashMap;
import java.util.Map;

@Service
public class CatalogService {

    @Autowired
    private CreditRepository creditRepository;

    @Autowired
    private ClientRepository clientRepository;

    public Map<String, String> getStatuses() {
        Map<String, String> statuses = new HashMap<String, String>();
        statuses.put("1", "Activo");
        statuses.put("2", "Inactivo");
        statuses.put("3", "Eliminado");

        return statuses;
    }

    public Map<String, String> getRoles() {
        Map<String, String> roles = new HashMap<String, String>();
        roles.put(Role.ADMINISTRATOR.getValue(), "Administrador");
        roles.put(Role.LOAN_OFFICER.getValue(), "Oficial de Crédito");
        // roles.put(Role.SUPPORT.getValue(), "Soporte");

        return roles;
    }

    public Map<String, String> getCredits() {
        Map<String, String> credits = new HashMap<String, String>();
        for(Credit credit:  creditRepository.findByStatus(Status.ACTIVE.getValue())) {
            credits.put(credit.getId(), credit.getName());
        }

        return credits;
    }

    public Map<String, String> getClients() {
        Map<String, String> clients = new HashMap<String, String>();
        for(Client client:  clientRepository.findByStatus(Status.ACTIVE.getValue())) {
            clients.put(client.getId(), client.getIdentityCard() + " · " + client.getFullName());
        }

        return clients;
    }

    public Map<String, String> getCities() {
        Map<String, String> cities = new HashMap<String, String>();

        cities.put("Latacunga", "Latacunga");
        cities.put("La Maná", "La Maná");
        cities.put("Pangua", "Pangua");
        cities.put("Pujilí", "Pujilí");
        cities.put("Salcedo", "Salcedo");
        cities.put("Saquisilí", "Saquisilí");
        cities.put("Sigchos", "Sigchos");

        return cities;
    }
}
