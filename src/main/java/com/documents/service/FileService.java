package com.documents.service;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileService {
    public static final String COLLECTION_NAME = "files";

    public String save(MultipartFile file) throws IOException {
        MongoClient mongo = new MongoClient("localhost");
        DB db = mongo.getDB("andina");

        GridFS grid = new GridFS(db, COLLECTION_NAME);
        GridFSInputFile inputFile = grid.createFile(multipartToFile(file));
        inputFile.setFilename(file.getOriginalFilename());
        // inputFile.set
        inputFile.save();

        return inputFile.getId().toString();
    }

    protected File multipartToFile(MultipartFile multipartFile) throws IllegalStateException, IOException {
        File file = new File(multipartFile.getOriginalFilename());
        multipartFile.transferTo(file);
        return file;
    }

    public GridFSDBFile retrieve(String id) throws IOException {
        MongoClient mongo = new MongoClient("localhost");
        DB db = mongo.getDB("andina");

        GridFS gridFS = new GridFS(db, COLLECTION_NAME);
        return gridFS.find(new ObjectId(id));
    }

    public void delete(String fileName) throws IOException {
        MongoClient mongo = new MongoClient("localhost");
        DB db = mongo.getDB("andina");

        GridFS grid = new GridFS(db, COLLECTION_NAME);
        grid.remove(fileName);
    }
}
