package com.documents.service;

import com.documents.domain.document.Template;
import com.documents.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class TemplateService {

    @Autowired
    private TemplateRepository templateRepository;

    public Template create(Template template) {
        template.setId(UUID.randomUUID().toString());
        template.setCreatedAt(new Date());
        return templateRepository.save(template);
    }

    public List<Template> findAll()
    {
        createDefaultTemplate();
        return templateRepository.findAll();
    }

    public Template findById(String id) {
        return templateRepository.findOne(id);
    }

    public Template findByType(String type) {
        return templateRepository.findByType(type);
    }

    public Template update(Template template) {
        Template instance = templateRepository.findOne(template.getId());
        if (instance == null) {
            return null;
        }

        // FIXME: This version will only have one type.
        // missing-documents
        instance.setName(template.getName());
        instance.setSubject(template.getSubject());
        instance.setMessage(template.getMessage());
        instance.setUpdatedAt(new Date());

        return templateRepository.save(instance);
    }

    private void createDefaultTemplate() {
        Template defaultTemplate = templateRepository.findByType("missing-documents");
        if (defaultTemplate == null) {
            Template template = new Template(
                    "",
                    "missing-documents",
                    "Documentos Faltantes",
                    "Documentos Faltantes",
                    "");
            create(template);
        }
    }
}
