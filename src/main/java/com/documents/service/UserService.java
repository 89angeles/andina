package com.documents.service;

import com.documents.domain.enumeration.Status;
import com.documents.domain.document.User;
import com.documents.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;
import sun.jvm.hotspot.debugger.linux.sparc.LinuxSPARCThreadContext;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private final static String INSTALLER_USER = "installer";

    @Autowired
    private UserRepository userRepository;

    public User create(User user) {

        user.setId(UUID.randomUUID().toString());
        user.setCreationDate(new Date());

        return userRepository.save(user);
    }

    public List<User> findAll() {

        List<User> users = userRepository.findAll();
        List<User> result = new LinkedList<User>();

        User installerUser = userRepository.findByUsername(INSTALLER_USER);

        for(User user : users) {
            if (!user.getId().equals(installerUser.getId())) {
                result.add(user);
            }
        }

        return result;
    }

    public User findById(String id) {

        return userRepository.findOne(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User update(User user) {
        User existingUser = userRepository.findOne(user.getId());
        if (existingUser == null) {
            return null;
        }

        existingUser.setIdentificationNumber(user.getIdentificationNumber());
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());
        existingUser.setStatus(user.getStatus());
        existingUser.setRole(user.getRole());
        if (!user.getPassword().trim().isEmpty()) {
            existingUser.setPassword(user.getPassword());
        }

        return userRepository.save(existingUser);
    }

    public Boolean delete(String id) {
        User existingUser = userRepository.findOne(id);
        if (existingUser == null) {
            return false;
        }

        // userRepository.delete(existingUser);
        existingUser.setStatus(Status.DELETED.getValue());
        userRepository.save(existingUser);
        return true;
    }

    public boolean userExists(String identificationNumber) {
        return userRepository.findByIdentificationNumber(identificationNumber) != null;
    }

    private String generateHash(String input) {
        String md5 = null;
        if (input == null) {
            return null;
        }

        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes(), 0, input.length());
            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException exception) {
            exception.printStackTrace();
        }

        return md5;
    }
}
