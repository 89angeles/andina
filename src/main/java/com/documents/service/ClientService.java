package com.documents.service;

import com.documents.domain.document.Client;
import com.documents.domain.enumeration.Status;
import com.documents.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public Client create(Client client) {
        client.setId(UUID.randomUUID().toString());
        client.setCreationDate(new Date());
        return clientRepository.save(client);
    }

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public Client findById(String id) {
        return clientRepository.findOne(id);
    }

    public Client update(Client client) {
        Client instance = clientRepository.findOne(client.getId());
        if (instance == null) {
            return null;
        }

        instance.setIdentityCard(client.getIdentityCard());
        instance.setFirstName(client.getFirstName());
        instance.setLastName(client.getLastName());
        instance.setEmail(client.getEmail());
        instance.setAddress(client.getAddress());
        instance.setStatus(client.getStatus());

        return clientRepository.save(instance);
    }

    public Boolean delete(String id) {
        Client instance = clientRepository.findOne(id);;
        if (instance == null) {
            return false;
        }

        instance.setStatus(Status.DELETED.getValue());
        clientRepository.save(instance);
        return true;
    }

    public boolean clientExists(String identityCard) {
        return clientRepository.findByIdentityCard(identityCard) != null;
    }
}
